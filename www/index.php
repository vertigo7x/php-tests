
<html>
 <head>
  <title>PHP TESTS</title>

  <meta charset="utf-8"> 

  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <h1>PHP TESTS</h1>
            </div>
        </div>
        <ul class="list-group">
            <li class="list-group-item"><a href="register.php">Registration</a></li>
            <li class="list-group-item"><a href="colors.html">Colors</a></li>
            <li class="list-group-item"><a href="responsive.html">Responsive</a></li>
        </ul>
    </div>
</body>
</html>
