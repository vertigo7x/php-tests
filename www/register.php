<?php  

    // Session
    session_start();

    // Global object connection
    $conn = new mysqli('db', 'user', 'test', "myDb");

    // TODO: POST Logic
    if (!empty($_POST)) {        
        // Make POST plain
        extract($_POST);
        // CSRF Token
        if (isset($_SESSION["token"])) {
            if (isset($_POST["csrf"])) {
                if ($_POST["csrf"] != $_SESSION["token"]) {
                    echo "CSRF";
                    exit();
                }
            }
        }
        // Check Javascript
        if (isset($_POST['support'])) {
            if (!$_POST['support']) {
                $_SESSION['message'] = "Please enable Javascript on your browser";
            } else {
                // Javascript validation passed
                // Check for existing email
                $query = $conn->prepare("SELECT 1 FROM Users Where email=?");
                $query->bind_param("s", $email);
                $query->execute();
                $query->bind_result($repeated_email);
                $query->fetch();
                $query->close();
                if ($repeated_email) {
                    $_SESSION['message'] = "The email provided is already taken.";
                } else {
                    $query = $conn->prepare("INSERT INTO Users (id, name, email, password, address1, address2, address3, city, region, country_uuid, heardof_uuid, heardother, terms) VALUES (UUID(), ?, ?, MD5(?), ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                    $query->bind_param("sssssssssssi", $name, $email, $password, $address1, $address2, $address3, $city, $region, $country, $heardof, $heardother, $terms);
                    $query->execute();
                    $_SESSION['logged_in'] = true;
                    header('location:success.php');
                }
            }
        }
    } else {
        $_SESSION['message'] = "";
    }

    // Regenerate token
    $_SESSION["token"] = md5(uniqid(mt_rand(), true));

    // Connection failure
    if (mysqli_connect_errno()) {
        echo "Failure on DB Connection: %s\n";
        exit();
    }

    $queryCountries = 'SELECT * FROM Countries';
    $queryHeardOf = ' SELECT * FROM Heardof';

    // Combos
    $resultCountries = $conn->query($queryCountries)->fetch_all(MYSQLI_ASSOC);
    $resultHeardOf = $conn->query($queryHeardOf)->fetch_all(MYSQLI_ASSOC);

    $conn->close();

    ?>
<html>
 <head>
  <title>REGISTER</title>

  <meta charset="utf-8"> 

  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>

</head>
<body>
    <div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>REGISTER FORM</h1>
            <p>Please fill in all the required information. Fields with * are required.</p>
            <noscript>Your browser doesn't support javascript or maybe it's disabled.
            Please, enable javascript or use other browser</noscript>
            <?php if (!empty($_SESSION['message'])): ?>
            <div class="alert alert-error">
                <strong>Error</strong> <?php echo $_SESSION['message']; ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <form name="register" id="register" method="POST" action="register.php">
        <input type="hidden" name="csrf" value="<?php echo $_SESSION["token"]; ?>" />
        <input type="hidden" name="support" id="js" value="0" />
        <div class="form-group">
            <label for="name"><sup>*</sup>Full Name:</label>
            <input class="form-control" required id="name" type="text" name="name" minlength="2" maxlength="50" value="<?php echo $name; ?>" />
        </div>
        <div class="form-group">
            <label for="email"><sup>*</sup>Email:</label>
            <input class="form-control" required id="email" type="email" name="email" maxlength="150" value="<?php echo $email; ?>" />
        </div>
        <div class="form-group">
            <label for="password"><sup>*</sup>Password:</label>
            <input class="form-control" required id="password" type="password" name="password" minlength="6" maxlength="20" />
        </div>
        <div class="form-group">
            <label for="password2"><sup>*</sup>Repeat Password:</label>
            <input class="form-control" required id="password2" type="password" name="password2" minlength="6" maxlength="20" />
        </div>
        <div class="form-group">
            <label><sup>*</sup>Address:</label>
            <input class="form-control" required id="address1" type="text" name="address1" minlength="2" maxlength="80" value="<?php echo $address1; ?>"/>
            <label>(optional)</label>
            <input class="form-control" id="address2" type="text" name="address2" minlength="2" maxlength="80" value="<?php echo $address2; ?>" />
            <input class="form-control" id="address3" type="text" name="address3" minlength="2" maxlength="80" value="<?php echo $address3; ?>" />
        </div>
        <div class="form-group">
            <label for="city"><sup>*</sup>City:</label>
            <input class="form-control" required id="city" type="text" name="city" minlength="2" maxlength="30" value="<?php echo $city; ?>" />
        </div>
        <div class="form-group">
            <label for="region"><sup>*</sup>Region:</label>
            <input class="form-control" required id="region" type="text" name="region" minlength="2" maxlength="30" value="<?php echo $region; ?>"/>
        </div>
        <div class="form-group">
            <label for="country"><sup>*</sup>County:</label>
            <select class="form-control" required id="country" name="country">
                <option value="">Choose one</option>
                <?php foreach($resultCountries as $item): ?>
                <option value="<?php echo $item['id']; ?>" <?php if($item['id'] == $country) echo "selected";?>><?php echo $item['name']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="heardof"><sup>*</sup>How you heard of us:</label>
            <select class="form-control" required id="heardof" name="heardof">
                <option value="">Choose one</option>
                <?php foreach($resultHeardOf as $item): ?>
                <option value="<?php echo $item['id']; ?>" <?php if($item['id'] == $heardof) echo "selected";?>><?php echo $item['name']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group" id="heardofother_group" style="display:<?php if(!empty($heardother)): ?>block;<?php else :?>none;<?php endif; ?>">
            <label for="heardofother"><sup>*</sup>Please write down how you heard of us:</label>
            <input class="form-control" id="heardofother" type="text" name="heardother" maxlength="100" value="<?php echo $heardother; ?>"/>
        </div>
        <div class="form-group" id="terms">
            <label for="terms">
            <sup>*</sup>I accept terms &amp; conditions
            </label>
            <input id="terms" required type="checkbox" name="terms" />
        </div>
        <button type="submit" class="btn btn-default">Register</button>
    </form>
  </div>
  <?php var_dump($_POST); ?>
</body>
</html>

<script type="text/javascript">
    $("#register").validate({
  rules: {
    password: "required",
    password2: {
      equalTo: "#password"
    }
  }
});
    // Toggle heardofother_group
    $(document).ready(function(){
        $("#js").val(1);
        var heardofother_group = $('#heardofother_group');
        var heardof = $('#heardofother');
        $('#heardof').change(function(){
            if ($(this).val() === 'other') {
                heardofother_group.show();
                heardof.prop('required',true)
            } else {
                heardofother_group.hide()
                heardof.prop('required', false);
            }
        });
    });
</script>
      