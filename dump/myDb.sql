SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

CREATE TABLE `Countries` (
  `id` VARCHAR(36) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Countries` (`id`, `name`) VALUES
('13237e92-0b8e-11e8-aafe-0242ac120002', 'Spain'),
('234e9dc1-0b8e-11e8-aafe-0242ac120002', 'UK');

CREATE TABLE `Heardof` (
  `id` VARCHAR(36) NOT NULL,
  `name`VARCHAR(100) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Heardof` (`id`, `name`) VALUES
('31ea97c6-0b8e-11e8-aafe-0242ac120002', 'Google Ads'),
('4c02b79b-0b8e-11e8-aafe-0242ac120002', 'Facebook Ads');

CREATE TABLE `Users` (
  `id` VARCHAR(36) NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `email` VARCHAR(150) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `address1` VARCHAR(80) NOT NULL,
  `address2` VARCHAR(80) NOT NULL,
  `address3` VARCHAR(80) NOT NULL,
  `city` VARCHAR(30) NOT NULL,
  `region` VARCHAR(30) NOT NULL,
  `country_uuid` VARCHAR(36) NOT NULL,
  `heardof_uuid` VARCHAR(36) NOT NULL,
  `heardother` VARCHAR(100) NOT NULL,
  `terms` boolean NOT NULL DEFAULT 0,
  PRIMARY KEY (id),
  FOREIGN KEY (country_uuid) REFERENCES Countries(id),
  FOREIGN KEY (heardof_uuid) REFERENCES Heardof(id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `Users` (`id`, `name`, `email`, `password`, `address1`, `address2`, `address3`, `city`, `region`, `country_uuid`, `heardof_uuid`, `heardother`, `terms`) VALUES
('d17c5084-0b8e-11e8-aafe-0242ac120002', 'Jesús Mendoza', 'jesusmendozapascual@gmail.com', MD5('hola'), 'C/ La X', '-', '-', 'Santa Cruz de Tenerife', 'Canary Islands', '234e9dc1-0b8e-11e8-aafe-0242ac120002', '4c02b79b-0b8e-11e8-aafe-0242ac120002', '', 1);
